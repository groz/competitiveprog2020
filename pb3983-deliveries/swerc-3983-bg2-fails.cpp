/*

SOLUTION in O(n) borrowed from https://blog.csdn.net/weixin_41380961/article/details/98733169

*/
#include<string.h>
#include<stdio.h>
#include<algorithm>
using namespace std;
const int M=100010;
int n,m;
int weight[M],origin[M],dis[M],q[M],dp[M],x[M],y[M];
int sum(int i)
{
    return dp[i]-dis[i+1]+origin[i+1];
}
int main()
{
    int t,w;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d%d\n",&m,&n);
        for(int i=1;i<=n;i++)
        {
            scanf("%d%d%d",&x[i],&y[i],&w);
            weight[i]=weight[i-1]+w;
            origin[i]=abs(x[i])+abs(y[i]);
            dis[i]=dis[i-1]+abs(x[i]-x[i-1])+abs(y[i]-y[i-1]);
        }
        int l=1,r=1;
        for(int i=1;i<=n;i++)
        {
            while(l<=r&&weight[i]-weight[q[l]]>m) ++l;
            dp[i]=sum(q[l])+dis[i]+origin[i];
            // printf("%d\n",q[l]);
            while(l<=r&&sum(i)<=sum(q[r])) --r;
            q[++r]=i;
        }
        printf("%d\n%s",dp[n],t>0?"\n":"");
    }
}
/* 
weight[i] is cumulative weight until packet i 
dis[i] is cumulative path until packet i (without counting returns)
origin[i] is just distance of i to origin (their program accepts negative grid coordinates)
dp[i] is best cost when returning just after packet i
q is a queue of possible last_returns, sorted by increasing capacity and sorted also by decreasing extra cost for the returns.
l,r are current bounds delimiting the candidates for last return to origin in q.
*/
