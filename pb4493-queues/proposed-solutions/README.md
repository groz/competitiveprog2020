# Presentation

The problems must be packaged accordingly to the
[Kattis Problem Format](http://www.problemarchive.org/).

It is proposed to have four directories ready to be used as
problem repositories:

- `proposed` for problems not yet formally accepted by the team
- `swerc` for problems to be used for SWERC 2017
- `training` for problems to be used during the training
- `extra` for extra problems to be used in case of emergency

# Installation

The tools to verify the problem can be installed locally (for
example in `$HOME/kattis`) by doing:

``` bash
% virtualenv -p python2 ~/kattis
% PATH=~/kattis/bin:$PATH
% pip install git+https://github.com/kattis/problemtools
```

Note that a valid LaTeX installation (with Cyrillic extra
packages) must also be available.

# Checking a problem

After that, going for example into `swerc`, one can verify
that a problem is correct:

```bash
% verifyproblem cordonbleu
Loading problem cordonbleu
Checking config
Checking statement
Checking validators
Checking graders
Checking data
Checking submissions
   AC submission sam-On3.cpp (C++) OK: AC [CPU: 0.02s @ test case secret/010]
   AC submission sam-On4.cpp (C++) OK: AC [CPU: 0.08s @ test case secret/029]
   AC submission xtof-On3.py (Python 3) OK: AC [CPU: 0.99s @ test case secret/010]
   Slowest AC runtime: 0.987, setting timelim to 5 secs, safety margin to 10 secs
cordonbleu tested: 0 errors, 0 warnings
```
