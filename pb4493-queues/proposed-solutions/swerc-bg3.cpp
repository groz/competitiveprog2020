/* Solution borrowed from
 https://fabhodev.wordpress.com/2014/07/19/12207-that-is-your-queue-uva-solution/
*/
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <list>

using namespace std;

list<int> lista;

void init(int N)
{
    lista.clear();
    int lim = min(N, 1000);
    for(int i=1; i<=lim; i++)
        lista.push_back(i);
}

int main()
{
    int dim, queries, pos, tmp, casos = 1;
    char ins;
    while(scanf("%d %d", &dim, &queries))
    {
        if(!dim && !queries)
           break;

        init(dim);
        printf("Case %d:\n", casos++);
        for(int i=1; i<=queries; i++)
        {
            cin>>ins;
            if(ins == 'N')
            {
                tmp = lista.front();
                printf("%d\n", tmp);
                lista.pop_front();

                //if(dim <= 1000)
                  lista.push_back(tmp);
            }
            else
            {
                scanf("%d", &pos);
                lista.remove(pos);
                lista.push_front(pos);
            }
        }
    }
return 0;
}