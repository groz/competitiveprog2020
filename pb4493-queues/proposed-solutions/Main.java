import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.lang.Comparable;
import java.util.HashMap;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		
		while(!(line=in.readLine()).equals("0 0")) {
			String[] data = line.split("\\s+");
			int pop_size = Integer.parseInt(data[0]);
			int opes = Integer.parseInt(data[1]);
			
			HashMap<Integer,Person> access = new HashMap<>();
			PriorityQueue<Person> pq = new PriorityQueue<>();
			for (int i=1;i<Math.min(pop_size+1,opes+1);i++) {
				Person p = new Person(i,i);
				access.put(i,p);
				pq.add(p);
			}
			
			int rank_counter = pop_size + 1;
			
			for (int i=0;i<opes;i++) {
				String[] s = in.readLine().split("\\s+");
				Person p;
				if (s[0].equals("N")) {
					p = pq.poll();
					System.out.println(p.id);
					p.rank = rank_counter;
					rank_counter++;
					pq.add(p);
				} else {
					int a = Integer.parseInt(s[1]);
					if (!access.containsKey(a)) {
						p = new Person(pq.peek().rank-1,a);
						access.put(a,p);
						pq.add(p);
					} else {
						p = access.get(a);
						pq.remove(p);
						if (pq.size()==0) {
							p.rank = 0;
						} else {
							p.rank = pq.peek().rank - 1;
						}
						pq.add(p);
					}
				}
			}
		}
		
	}
	
}

class Person implements Comparable {
	int rank,id;
	
	public Person(int rank, int id) {
		this.rank = rank;
		this.id = id;
	}
	
	@Override
	public int compareTo(Object p) {
        return this.rank - ((Person)p).rank;
    }
}