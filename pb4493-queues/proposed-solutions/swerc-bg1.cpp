#include <iostream>
#include <cstdio>

#include <string>
#include <vector>
#include <list>
#include <unordered_map>

#include <numeric>


using namespace std;


typedef list<int>::iterator itr;
typedef unordered_map<int,itr>::iterator mapiter;

int serial = 1;
int nb_pers = 1;
int nb_op;
vector<int> inst;

void input() {
	inst.clear();
	cin >> nb_pers >> nb_op;
	char c;
	int citz;
	for(int i = 0; i < nb_op; i++) {
		cin >> c;
		if(c == 'N')
			inst.push_back(-1);
		else {
			cin >> citz;
			inst.push_back(citz);
		}
	}
}

int compute() {
	/* General idea: Maintain the queue as a list. 
	Map a gives location of citizen i in list.
	For N: is O(1) operations on list, possibly 1 insertion into hashmap
	For E x: O(1) operations on list and 1 hashmap update.
	*/
	
	unordered_map<int,itr> a; // a[i] is current location of citizens i in queue

	int qsize = min(nb_pers, nb_op); // by assumption > 0
	list<int> q(qsize);
	iota(q.begin(), q.end(), 1);	// initialize list
	itr current = q.begin(); // current position in queue
	
	for (itr i = q.begin(); i != q.end(); ++i) {
		a[*i] = i;
	}
	
	for (int i : inst) {
		if (i == -1) {
			if (current == q.end()) {
				current = q.begin();
			}
			cout << *current << endl;
			++current;
		}
		else {
			mapiter search = a.find(i);
			if (search != a.end()) {
				if (search->second != current) {
					q.erase(search->second);
					itr insertpos = q.insert(current, i);
					a[i] = insertpos;
					current--;
				}
			}
			else {
				itr insertpos = q.insert(current, i);
				a[i] = insertpos;
				current--;
			}
		}
	}
	return -1;
}

int main() {
	std::ios::sync_with_stdio(false);
    input();
	while (nb_pers > 0) {
		cout << "Case " << serial << ":" << endl;
		if (nb_op > 0) {// get rid of empty case. But should not happen.
			compute();
		}
		serial++;
		input();
	}
	return 0;
}
