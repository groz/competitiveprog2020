#include <iostream>
#include <cstdio>

#include <algorithm>

#include <string>
#include <vector>
#include <unordered_set>

#include <numeric>



using namespace std;


typedef unordered_set<int>::iterator setiter;

int serial = 1;
int nb_pers = 1;
int nb_op;
vector<int> inst;


void debug(string s) {
	cout<<s<<endl;
}

void input() {
	inst.clear();
	cin >> nb_pers >> nb_op;
	// cout<<nb_pers<<' '<<nb_op<<endl;
	char c;
	int citz;
	for(int i = 0; i < nb_op; i++) {
		cin >> c;
		if(c == 'N')
			inst.push_back(-1);
		else {
			cin >> citz;
			inst.push_back(citz);
		}
	}
}

int compute_case_nb_op_smaller() {
	/* General idea: There cannot be a cycle (if we see citizen i twice, it is because he was recalled by E i)
	Maintain the queue as a stack. 
	unordered_set visit indicates which citizens must be skipped by N (because alreadi recalled by E).
	For N: pop from stack until unvisited, update visit: O(1) (amortized)
	For E x: push on top of stack, update visit: O(1).
	*/
	
	vector<int> q; // q.back() is next_cit in queue (unless visited)
	unordered_set<int> visited; // visited.contains(i) if we already visited current version of i.
	
	int next_cit = 1;

	
	for (int i : inst) {
		
		if (i == -1) {
			while(!q.empty() && visited.find(q.back()) != visited.end()) {
				q.pop_back();
			}
			if (q.empty()) {
				while(visited.find(next_cit) != visited.end()) {
					next_cit++;
				}
				visited.insert(next_cit);
				cout << next_cit << endl;
				next_cit++; // optional.
			}
			else {
				visited.insert(q.back());
				cout << q.back() << endl;
				q.pop_back(); // optional.
			}
		}
		else {
			q.push_back(i);
			visited.erase(i);
		}
	}
	return -1;
}

int compute_case_nb_op_larger() {
	/* General idea: 
	same as above using a kind of double-ended queue: in fact this is more general case that could be used for both.
	However, if we use this one only for nb_op_larger, we could optimize visited to make it a bitset - but then adapt accessors, etc.
	*/
	
	vector<int> q; // q.back() is next_cit in queue (unless visited)
	vector<int> q2; // q2.front() is next_cit in queue (unless visited)

	unordered_set<int> visited; 
	
	int next_cit = 1;

	
	for (int i : inst) {
		if (i == -1) {
			while(!q.empty() && visited.find(q.back()) != visited.end()) {
				q.pop_back();
			}
			if (q.empty()) {
				while(next_cit <= nb_pers && visited.find(next_cit) != visited.end()) {
					next_cit++;
				}
				if (next_cit <= nb_pers) {
					visited.insert(next_cit);
					cout << next_cit << endl;
					q2.push_back(next_cit);
					next_cit++; // not optional unless we remember if a "flush q2 into q"" already happened.
				}
				else { // it is time to flush q2 into q, skipping irrelevant items and inverting order.
					visited.clear();
					q.clear();
					for (auto cit = q2.rbegin(); cit != q2.rend(); ++cit) {
						if (visited.find(*cit) == visited.end()) {
							q.push_back(*cit);
							visited.insert(*cit);
						}
					}
					q2.clear();
					visited.clear();
					// starting anew with a fresh (non-empty) queue q.
					q2.push_back(q.back());
					visited.insert(q.back());
					cout << q.back() << endl;
					q.pop_back(); // optional.
				}
			}
			else {
				q2.push_back(q.back());
				visited.insert(q.back());
				cout << q.back() << endl;
				q.pop_back(); // optional.
			}
		}
		else {
			q.push_back(i);
			visited.erase(i);
		}
	}
	return -1;
}

int main() {
	std::ios::sync_with_stdio(false);
    input();
	while (nb_pers > 0) {
		cout << "Case " << serial << ":" << endl;
		if (nb_op <= nb_pers) {// we could use nb_op_large in both cases instead.
			compute_case_nb_op_smaller();
		}
		else {
			compute_case_nb_op_larger();
		}
		serial++;
		input();
	}
	return 0;
}