#include <vector>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unordered_map>
#include <algorithm>
#include <deque>
#include <set> 
#include <list> 
#include <map> 
#include <unordered_map> 

using namespace std;

//ios::sync_with_stdio(false);


int nbPers;
int nbOp;
vector<int> instruction; // -1 -> read; other indice de l'urgence 
void input()
{
	instruction.clear();
	cin >> nbPers >> nbOp;
	char c;
	for(int i=0; i < nbOp; i++)
	{
		cin >> c;
		if(c == 'N')
		{
			instruction.push_back(-1);
		}
		else
		{
			int urg;
			cin >> urg;
			instruction.push_back(urg);
		}	
	}
}



int main()
{
	
	int nbCase = 1;
	while (true)
	{

		input();
		if(nbOp == 0 && nbPers ==0)
		{
			break;
		}
		cout<< "Case " << nbCase <<  ":\n";


		list<int> file;
		unordered_map<int,list<int>::iterator> position;
		for(int i=1 ; i<min(nbOp,nbPers)+1; i++)
		{
			
			file.push_back(i);
			position[i]= prev(file.end(), 1);
			

		}

		for(int instru : instruction)
		{
			
			if(instru == -1)
			{
				cout << file.front() << "\n";
				file.push_back(file.front());
				position[file.front()] = prev(file.end(), 1);
				file.pop_front();
			}
			else
			{
				file.erase(position[instru]);
				file.push_front(instru);
				position[instru] = file.begin();

			}
			
		}
		nbCase ++;


	}




	return 0;
}