#include <iostream>
#include <iterator>
#include <list>
#include <vector>
#include <unordered_map>

using namespace std;

int P, C;

std::list<int> patQueue;
std::unordered_map<int, std::list<int>::iterator> patRef;

void readInput()
{
  for (int caseId = 1; ; caseId++) {
    cin >> P >> C;
    if (P == 0 && C == 0) { return; }
    cout << "Case " << caseId << ":\n";
    for (int i = 0; i < std::min(P, C); i++) {
      patQueue.emplace_back(i);
      patRef[i] = std::prev(patQueue.end(), 1);
    }
    for (int i = 0; i < C; i++) {
      char command;
      cin >> command;
      if (command == 'E') {
        int expedite;
        cin >> expedite;
        expedite--; // Conversion to 0-based
        patQueue.push_front(expedite);
        if (patRef.find(expedite) != patRef.end()) { // Patient is already in the queue, remove it
          patQueue.erase(patRef[expedite]);
        }
        patRef[expedite] = patQueue.begin();
      } else { // Treat the patient at the front of the queue then move it to the back
        cout << patQueue.front() + 1 << endl; // Conversion to 1-based
        patQueue.emplace_back(patQueue.front());
        patQueue.pop_front();
        patRef[patQueue.back()] = std::prev(patQueue.end(), 1);
      }
    }
    patQueue.clear();
    patRef.clear();
  }
}

int main()
{
  readInput();
  return 0;
}
