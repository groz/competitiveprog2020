// #include <bits/stdc++.h>
#include "headerallincludes.h"

using namespace std;

struct stone{
int x, y, w, h, a ,b;
};

typedef int lli;

vector<lli> sommes(50000, 0);
vector<int> ranks(50000, 0); int c[50000];
vector<stone> s;

int Find(int x)
{
    //cout << "for " << x;
    int y = x;
    while(y!=c[y])
        y = c[y];
    while(x != c[x])
    {
        int aux = c[x];
        c[x] = y;
        x = aux;
    }
    //cout << " found " << y << '\n';
    return y;
}

void Union(int x, int y)
{
    if(ranks[x] > ranks[y])
        c[x] = y;
    else
        c[y] = x;
    if(ranks[x]==ranks[y])
        ranks[y]++;
}

bool colliz(int i, int j)
{
    if( (min(s[i].a, s[j].a) >= max(s[i].x, s[j].x)) && (min(s[i].b, s[j].b) >= max(s[i].y, s[j].y)) )
        {/*cout << i << "et" << j << '\n'*/; return true;}
    return false;
}

int main()
{
    int n;
    while(cin >> n) {
	    s = vector<stone>(n);

	    for(int i = 0; i < n; i++)
	        c[i] = i;

	    for(int i = 0; i < n; i++)
	    {
	        cin >> s[i].x >> s[i].y >> s[i].w >> s[i].h;
	        s[i].a = s[i].x+s[i].w;
	        s[i].b = s[i].y+s[i].h;
	    }

	    for(int i = 0; i<n; i++)
	    {
	        for(int j = i+1; j<n; j++)
	        {
	            if(colliz(i, j))
	                Union(Find(i), Find(j));
	        }
	    }
	    for(int i = 0; i < n; i++)
	        sommes[Find(i)] += (s[i].w)*(s[i].h);
	    lli best = 0;
	    for(int i = 0; i < n; i++)
	        best = max(sommes[i], best);
	    cout << best <<endl;

		fill(sommes.begin(), sommes.begin()+n, 0);
		fill(ranks.begin(), ranks.begin()+n, 0);
		// s.clear(); useless
    }

    return 0;

}
