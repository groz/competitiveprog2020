#include <iostream>
#include <cstdio>

#include <algorithm>

#include <string>
#include <vector>
#include <unordered_set>

#include <numeric>



using namespace std;

struct box {
	int minx, miny, maxx, maxy, a, id;
}


vector<int> anc;
vector<int> rk;
vector<box> boxes;

int find(int x) {
	int y = x;
	while (anc(y)!= y)
	{
		y = anc(y);
	}
	anc(x) =y;
	return y;
}

void union(int x, int y)
{
	fx = find(x);
	fy = find(y);
	if rk(fx) < rk(fy)
	{
		anc(fx) = fy;
	}
	else if (rk(fx) == rk(fy))
	{
		anc(fx) = fy;
		rk(fy)++;
	}
	else {
		anc(fy) = fx;
	}
}

bool colliz(box x, box y) {
	return min(x.maxx, y.maxx) >= max(x.minx, y.minx) &&	min(x.maxy, y.maxy) >= max(x.miny, y.miny);
}


void input() {
	inst.clear();
	cin >> nb_pers >> nb_op;
	// cout<<nb_pers<<' '<<nb_op<<endl;
	char c;
	int citz;
	for(int i = 0; i < nb_op; i++) {
		cin >> c;
		if(c == 'N')
			inst.push_back(-1);
		else {
			cin >> citz;
			inst.push_back(citz);
		}
	}
}

int main() {
	std::ios::sync_with_stdio(false);
    input();
	while (nb_pers > 0) {
		cout << "Case " << serial << ":" << endl;
		if (nb_op <= nb_pers) {// we could use nb_op_large in both cases instead.
			compute_case_nb_op_smaller();
		}
		else {
			compute_case_nb_op_larger();
		}
		serial++;
		input();
	}
	return 0;
}