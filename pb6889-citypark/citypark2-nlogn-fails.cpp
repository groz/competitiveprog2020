// #include <bits/stdc++.h>
#include "headerallincludes.h"


using namespace std;


typedef long long LL;

struct Segm {
	LL smin, smax, c, box_id;
};

struct SegmGreater
{
    bool operator()( const Segm& ls, const Segm& rs ) const {
		// lexicographic comparison.
        return forward_as_tuple(ls.c,ls.smin,ls.smax) < forward_as_tuple(rs.c,rs.smin,rs.smax);
    }
};

vector<LL> ba(50000,0); // ba[i] = area of box i
vector<vector<Segm>> zs(2, vector<Segm>(100000)); // zs[0] = xsegments of boxes. zs[1] = ysegments
vector<LL> anc(50000,0); // union find anc
vector<LL> rk(50000,0); // union find rk
vector<LL> sommes(50000,0);

LL uf_find(LL x) {
	LL y = x;
	// find the root
	while (anc[y] != y)
	{
		y = anc[y];
	}
	// compress path
	while (x != y)
	{
		LL aux = anc[x];
		anc[x] = y;
		x = aux;		
	}
	return y;
}

void uf_union(LL x, LL y)
{
	LL fx = uf_find(x);
	LL fy = uf_find(y);
	if (fx != fy) {
		if (rk[fx] < rk[fy])
		{
			anc[fx] = fy;
		}
		else
		{
			anc[fy] = fx;
			if (rk[fx] == rk[fy])
				rk[fx]++;
		}
	}
}


void input(LL n) {
    for(LL i = 0; i < n; i++)
        anc[i] = i;

    for(LL i = 0; i < n; i++)
    {
		LL x,y,w,h;
        cin >> x >> y >> w >> h;
        ba[i] = w * h;
		zs[0][2*i] = {x, x+w, y, i};
		zs[0][2*i+1] = {x, x+w, y+h, i};
		zs[1][2*i] = {y, y+h, x, i};
		zs[1][2*i+1] = {y, y+h, x+w, i};
    }
}

int main() {
	std::ios::sync_with_stdio(false);
	LL n;
	while(cin >> n) {
	    input(n);
		/* merging partitions that are adjacent on x or y */
		for (auto &xs: zs) { // xs is xsegments first time, and ysegment the second. But same operations for both.
			sort(xs.begin(), xs.begin()+2*n, SegmGreater());
			LL lb = xs[0].box_id;
			LL lx = xs[0].smax;
			LL ly = xs[0].c;
			for(LL i=1; i < 2*n; i++) {
				// cout<<"consider "<<lb<<" and "<<xs[i].box_id<<endl;
				if (xs[i].c == ly &&  xs[i].smin <= lx)
					uf_union(lb, xs[i].box_id);
				if (xs[i].c == ly) {
					lx = max(lx, xs[i].smax);
				}
				else {
					lb = xs[i].box_id;
					lx = xs[i].smax;
					ly = xs[i].c;
				}
			}
		}
		
		/* compute area of each partition */
		for (LL i=0; i<n; i++) {
			sommes[uf_find(i)] += ba[i];
		}
		LL best = *max_element(sommes.begin(), sommes.begin()+n);
	    cout << best << endl;

		fill(sommes.begin(), sommes.begin()+n, 0);
		fill(rk.begin(), rk.begin()+n, 0);
		// No need to reset the others: the useful portion is reset by program.
		// fill(ba.begin(), ba.begin()+n, 0);
		// fill(anc.begin(), anc.begin()+n, 0);
		// zs[0]= *new vector<Segm>(100000);
		// zs[1]= *new vector<Segm>(100000);
	}
	return 0;
}