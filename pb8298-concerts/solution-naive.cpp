#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>

using namespace std;

#define MODULUS 1000000007

int n, k;
int hb[26];
std::vector<char> seqk;
std::vector<char> seqn;
std::vector<int> solution;

inline void solve()
{
  solution[0] = (seqk[0] == seqn[0]);
  for (int in = 1; in < n; in++) { solution[in] = solution[in - 1] + (seqk[0] == seqn[in]); }
  char prevConcert = seqk[0] - 'A';
  for (int ik = 1; ik < k; ik++) {
    const char curConcert = seqk[ik] - 'A';
    int in;
    for (in = n - 1; in >= std::max(ik, hb[prevConcert]); in--) {
      solution[in] = (seqk[ik] == seqn[in]) * solution[in - hb[prevConcert]];
    }
    for (in = 0; in < std::max(ik, hb[prevConcert]); in++) { solution[in] = 0; }
    for (; in < n; in++) { solution[in] = (solution[in] + solution[in - 1]) % MODULUS; }
    prevConcert = curConcert;
  }
}

inline bool readInput()
{
  cin >> k; if (cin.eof()) { return false; }
  cin >> n;
  if (k > seqk.size()) { seqk.resize(k); }
  if (n > seqn.size()) { seqn.resize(n); solution.resize(n); }
  
  char c;
  for (int i = 0; i < 26; i++) { cin >> hb[i]; hb[i]++; }
  for (int i = 0; i < k; i++) { cin >> seqk[i]; }
  for (int i = 0; i < n; i++) { cin >> seqn[i]; }
  return true;
}

int main()
{
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  while (readInput())
  {
    solve();
    printf("%d\n", solution[n - 1]);
  }
  return 0;
}
