#include <iostream>
#include <cstdio>
#include <iomanip>

#include <string>
#include <vector>
#include <cmath>

#include <numeric>
#include <algorithm>

using namespace std;


/* This is a very naive O(n log(n) + s * log(precision)) version.
 
 Idée: on appelle slice un intervalle sur z, chaque trou délimitant 3 hauteurs coupant les intervalles: le bas du trou, le centre et le haut.
 Le volume enlevé depuis leur début jusqu'au niveau z par l'ensemble des trous encore présents au niveau z est donné par un polynome de degré 3 en z.
 Le polynome de degré 3 d'une slice se déduit facilement de celui de la slice précédente (en ajoutant les modifs apportées par les trous ayant provoqué le découpage à ce niveau).
 
 On commence donc par identifier la slice  dans laquelle on va couper notre tranche de fromage, puis on identifie à quelle hauteur dans cette slice par dichotomie (plutôt que d'inverser un polynome de degré 3)
 */


const long double pi = std::acos(-1);
const long double epsilon = 1e-14;

struct Hole
{	// coordinates are in micrometers, volume in cubic millimeters.
    const int r;
    const int x;
    const int y;
    const int z;
    const long double vol;
};

struct Hole_info
{
    long double z_min;
    long double coeffz3;
    long double coeffz2;
    long double coeffz1;
    long double coeffz0;
	long double completed_vol; // volume of the hole when the hole_info is for the termination (above boundary) of a hole, and 0 if not a termination.
};

struct Cum_slice
{
    long double z_min;
    long double coeffz3;
    long double coeffz2;
    long double coeffz1;
    long double coeffz0;
    long double cum_completed; // cumulative volume of holes terminated <= z_min. 
	// Remark: cum_completed could technically be merged into coeffz0 to improve efficiency a bit. But it's easier to debug when at least one field has some "real world" semantics.
};

string to_string(Hole h) {
    return to_string(h.r)+' '+to_string(h.x)+' '+to_string(h.y)+' '+to_string(h.z)+' '+to_string(h.vol);
}
string to_string(Hole_info h) {
    return "hole info ("+to_string(h.z_min)+' '+to_string(h.coeffz3)+' '+to_string(h.coeffz2)+' '+to_string(h.coeffz1)+' '+to_string(h.coeffz0)+' '+to_string(h.completed_vol)+')';
}
string to_string(Cum_slice h) {
    return "cum slice ("+to_string(h.z_min)+' '+to_string(h.coeffz3)+' '+to_string(h.coeffz2)+' '+to_string(h.coeffz1)+' '+to_string(h.coeffz0)+' '+to_string(h.cum_completed)+')';
}


/* 
Usual assumptions for binary search on comp and b: 
comp(x,v) = true when x < expected result, false when x > expected result 
*/
template<class BinaryPredicate>
long double bin_search(long double low, long double hi, long double v, long double eps, BinaryPredicate comp) {
    while (hi - low > eps) {
        long double mid = (low + hi)/2.;
        if (comp(mid,v)) {
            low = mid;
        }
        else {
            hi = mid;
        }
    }
    return low;
}

/* returns volume removed by holes that are not yet complete at level z. Assumes that z is really within the slice whose coefficients are used. 
So: current_slice_z_min <= z <= next slice's z_min */
long double volume_removed_in_slice(long double z, long double coeffz3, long double coeffz2, long double coeffz1, long double coeffz0) {
	return coeffz3 * pow(z,3) + coeffz2 * pow(z,2) + coeffz1 * z + coeffz0;
}

/* returns volume if we split the slice cs at z (assumes that z is really within that slice
So: current_slice_z_min <= z <= next slice's z_min */
long double halfspace_volume(long double z, vector<Cum_slice>::iterator cs) {
    return 1e4 * z - cs-> cum_completed - volume_removed_in_slice(z, cs->coeffz3, cs->coeffz2, cs->coeffz1, cs->coeffz0);
}


int nb_holes = -1;
int nb_slices = -1;
vector<Hole> inst;

void input() {
    inst.clear();
    cin >> nb_holes >> nb_slices;
    int r,x,y,z;
    for(int i = 0; i < nb_holes; i++) {
        cin >> r >> x >> y >> z;
        inst.push_back({r,x,y,z,4./3. * pi * pow(r/1000.,3)});
    }
}

int compute() {
    long double total_holes = 0;
    for(auto h : inst) {
        total_holes += h.vol;
    }
    long double total_vol = 1e6 - total_holes;
	
    if (nb_holes == 0) {
        for (int i = 0; i < nb_slices; i++) {
            cout << 100./nb_slices << endl;
        }
    }
    else {
        vector<Hole_info> hole_infos;
        vector<Cum_slice> cum_slices;
		
        for (auto h : inst) {
            if (h.r != 0) {
                long double r = h.r/1000., z = h.z/1000.;
                long double coeffz3 = -pi/3.;
                long double coeffz2 = pi*z;
                long double coeffz1 = -pi*(z-r)*(z+r);
                long double coeffz0low = pi/3. * pow(z-r,2) * (2 * r + z);
                long double coeffz0hi = pi/3. * (4 * pow(r,3) - pow(z+r,2) * (2 * r - z));
                hole_infos.push_back({z-r, coeffz3, coeffz2, coeffz1, coeffz0low, 0});
                if (z+r <= 1e2) {// no need to add a "correction to reset coeffs above the sphere" in that case
                    hole_infos.push_back({z+r, -coeffz3, -coeffz2, -coeffz1, -coeffz0hi, 4./3. * pi * pow(r,3)});
                }
            }
        }
        
        sort(hole_infos.begin(), hole_infos.end(), [](const Hole_info& hs1, const Hole_info& hs2) {return hs1.z_min < hs2.z_min;});
        long double cum_complet = 0, z_min = 0, coeffz3 = 0, coeffz2 = 0, coeffz1 = 0, coeffz0 = 0;
        auto s = hole_infos.begin();
        if (s->z_min > epsilon) {
            cum_slices.push_back({z_min, coeffz3, coeffz2, coeffz1, coeffz0, cum_complet});
        }
        while(s != hole_infos.end()) {
            long double z_min = s->z_min;
            while(s != hole_infos.end() && abs(s->z_min - z_min) < epsilon) {
                coeffz3 += s->coeffz3;
                coeffz2 += s->coeffz2;
                coeffz1 += s->coeffz1;
                coeffz0 += s->coeffz0;
				cum_complet += s->completed_vol;
                s++;
            }
            cum_slices.push_back({z_min, coeffz3, coeffz2, coeffz1, coeffz0, cum_complet});
        }
        if (abs(1e2 - cum_slices.back().z_min) > epsilon) {cum_slices.push_back({1e2,0,0,0,0,total_holes});} // used in binary search to obtain sentinel value for z_min, cum_complet.
        
        long double prev_z = 0.;
        long double new_z = 0.;
        auto cs = cum_slices.begin();
        for (int i = 1; i < nb_slices; i++) {
            long double objective = i * total_vol / nb_slices;
            while(halfspace_volume(cs->z_min, cs) < objective) {
                cs++;
            }
            long double next_z = cs->z_min;
            cs--;
            //cs is now the slice where we find our splitting boundary.
            new_z = bin_search(cs->z_min,
                               next_z,
                               objective,
                               1e-8,
                               [&cs](long double z, long double v) {
                                   return halfspace_volume(z,cs) < v;
                               });
            cout << new_z - prev_z<<endl;
            prev_z = new_z;
        }
        cout << 1e2 - prev_z<<endl;
    }
    return 0;
}

int main() {
    std::ios::sync_with_stdio(false);
    cout.precision(12);
    input();
    while (nb_holes >= 0 or nb_slices >= 0) {
        compute();
        nb_holes = -1;
        nb_slices = -1;
        input();
    }
    return 0;
}
