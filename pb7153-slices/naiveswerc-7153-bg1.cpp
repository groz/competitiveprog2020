#include <iostream>
#include <cstdio>
#include <iomanip>

#include <string>
#include <vector>
#include <cmath>

#include <numeric>
#include <algorithm>

/* This is a very naive O(n * s * log(precision)) version. 
Only use for small inputs.
However, focussing the binary search should not help much. So what we should do is avoid enumerating all holes for each slice

Pas assez rapide pour icpc. 
Idée: pour chaque tranche, on fait une dichotomie sur la hauteur. Pour chaque hauteur suggérée, on  traverse la liste de tous les "trous"  afin de calculer le volume qu'on obtiendrait en coupant à cette hauteur..
*/

using namespace std;


const long double pi = std::acos(-1);
// const long double epsilon = 1e-14;

struct hole
{	// coordinates and volume in cubic millimeters.
	const long double r;
	const long double x;
	const long double y;
	const long double z;
	const long double vol;
};
string to_string(hole h) {
	return to_string(h.r)+' '+to_string(h.x)+' '+to_string(h.y)+' '+to_string(h.z)+' '+to_string(h.vol);
}



template<class BinaryPredicate>
long double bin_search(long double low, long double hi, long double v, long double eps, BinaryPredicate comp) {
	while (hi - low > eps) {
		long double mid = (low + hi)/2.;
		if (comp(mid,v)) {
			low = mid;
		}
		else {
			hi = mid;
		}
	}
	return low;
}

// returns volume if we split the slice at z (assumes that z is really within that slice, so must be lower than next slice's z_min)
long double evaluate_hole_volume(long double z, const hole& h) {
	if (h.z-h.r>=z) {
		return 0.;
	}
	else if (h.z+h.r<=z) {
		return 4./3.*pi*pow(h.r,3);
	}
	else if (h.z> z) { // case z in lower half of sphere
			return pi/3.*pow(z-h.z+h.r,2)*(3*h.r-(z-h.z+h.r));
		}
	else { // case z in upper half of sphere
		return pi/3.*(4*pow(h.r,3)-(pow(h.z+h.r-z,2)*(3*h.r-(h.z+h.r-z))));
	}
}

long double evaluate_list_hole_volume(long double z, const vector<hole>& v) {
	long double vol = 0.;
	for (auto h: v) {
		vol += evaluate_hole_volume(z,h);
	}
	return vol;
}


int nb_holes = -1;
int nb_slices = -1;
vector<hole> inst;

void input() {
	inst.clear();
	cin >> nb_holes >> nb_slices;
	int r,x,y,z;
	for(int i = 0; i < nb_holes; i++) {
		cin >> r >> x >> y >> z;
		inst.push_back({r/1000.,x/1000.,y/1000.,z/1000.,4./3. * pi * pow(r/1000.,3)});
	}
}

int compute() {
	long double total_holes = 0;
	for(auto h : inst) {
		total_holes += h.vol;
	}
	long double total_vol = 1e6 - total_holes;
	if (nb_holes == 0) {
		for (int i = 0; i < nb_slices; i++) {
			cout << 100./nb_slices << endl;
		}
	}
	else {
		long double prev_z = 0.;
		long double new_z = 0.;
		for (int i = 1; i < nb_slices; i++) {
			long double objective = i * total_vol / nb_slices;
			new_z = bin_search(0., 
				100., 
				objective, 
				1e-10,
				[] (long double z, long double obj) {return z*1e4-evaluate_list_hole_volume(z,inst)<obj;});
			cout << new_z - prev_z<<endl;
			prev_z = new_z;
		}
		cout << 1e2 - prev_z<<endl;		
	}
	return 0;
}

int main() {
	std::ios::sync_with_stdio(false);
	cout.precision(12);
    input();
	while (nb_holes >= 0 or nb_slices >= 0) {
		compute();
		nb_holes = -1;
		nb_slices = -1;
		input();
	}
	return 0;
}
